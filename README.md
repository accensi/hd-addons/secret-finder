### Notes
---
- This mod requires [AceCoreLib](https://gitlab.com/accensi/hd-addons/acecorelib).
- The item can spawn either in backpacks or be found randomly in the wild in place of computer area maps.
- Loadout code is `fdr`.
- The higher the range, the faster battery is drained.